var app = angular.module('app',['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/');
  var loginState = {
    name: 'login',
    url: '/',
    templateUrl: 'components/login/views/indexView.html',
    controller: 'LoginCtrl'
  }

  $stateProvider.state(loginState);
});

console.log("app angular: ", app);

app.controller('GrandCtrl', ['$scope', function ($scope) {
	$scope.saludo = "Hola Mundo II";
	$scope.tab 	  = 'home';


	$scope.changeTab 	= function (status){
		$scope.tab = status;
	}
}])